//
//  ApiManager.swift
//  Test1
//
//  Created by Guillermo Guizar on 7/2/19.
//  Copyright © 2019 Scotiabank. All rights reserved.
//

import Foundation
import SwiftyJSON
import ObjectMapper

class ApiManager {
    /// Get data
    ///
    /// - Parameter success: Response json object ready to be parsed
    func getData(success: @escaping (_ response: Stations) -> Void) {
        let path = Bundle.main.path(forResource: "ecobici", ofType: "json")!
        do {
            let jsonString = try String(contentsOfFile: path, encoding: .utf8)
            if let dict = try JSONSerialization.jsonObject(with: jsonString.data(using: .utf8)!, options: .allowFragments) as? [String: Any] {
                if let stations = Stations(JSON: dict) {
                    success(stations)
                }
            }
        } catch {
            print("\(error.localizedDescription)")
        }
    }
}
