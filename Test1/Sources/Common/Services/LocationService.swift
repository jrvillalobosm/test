//
//  LocationService.swift
//  Test1
//
//  Created by appleDeveloper on 29/03/20.
//  Copyright © 2020 Scotiabank. All rights reserved.
//

import Foundation
import MapKit
import UIKit

class LocationService: NSObject {
    private var locationManager: CLLocationManager = CLLocationManager()
    var delegate: CLLocationManagerDelegate?

    override init() { }

    func start() {
        _ = CLLocationManager.authorizationStatus()
        if !CLLocationManager.locationServicesEnabled() {
            let title = Constants.locationEnabledTitle
            let message = Constants.locationEnabledMsg
            let actions: [AlertActions] = [
                    .standard(Constants.settingsAction, { (_) -> Void in
                        PreferenceType.locationPrivacity.openSettings()
                    }),
                    .cancel(Constants.cancelAction, nil)
            ]
            Alert().showAlert(title: title, message: message, actions: actions, withPreferredAction: true)
        } else if CLLocationManager.authorizationStatus() == .denied {
            let title = Constants.locationDeniedTitle
            let message = Constants.locationDeniedMsg
            let actions: [AlertActions] = [
                    .standard(Constants.changeSettingsAction, { (_) -> Void in
                        PreferenceType.locationPrivacity.openSettings(defaultSettings: true)
                    }),
                    .cancel(Constants.cancelAction, nil)
            ]
            Alert().showAlert(title: title, message: message, actions: actions, withPreferredAction: true)
        } else {
            startUpdatingLocation()
        }
    }

    public func stop() {
        locationManager.stopUpdatingLocation()
    }

    private func startUpdatingLocation() {
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.delegate = delegate
        locationManager.allowsBackgroundLocationUpdates = true
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
    }
}
