//
//  AlertManager.swift
//  Test1
//
//  Created by appleDeveloper on 29/03/20.
//  Copyright © 2020 Scotiabank. All rights reserved.
//

import Foundation
import UIKit

typealias AlertAction = (UIAlertAction) -> Void

enum AlertActions {
    case standard(String, AlertAction?)
    case cancel(String, AlertAction?)
    case destructive(String, AlertAction?)

    public var action: UIAlertAction {
        switch self {
        case .standard(let title, let handler):
            return UIAlertAction(title: title, style: .default, handler: handler)
        case .cancel(let title, let handler):
            return UIAlertAction(title: title, style: .cancel, handler: handler)
        case .destructive(let title, let handler):
            return UIAlertAction(title: title, style: .destructive, handler: handler)
        }
    }
}

class Alert {
    private let rootController: UIViewController? = {
        let app = UIApplication.shared.delegate as! AppDelegate
        return app.window?.rootViewController
    }()

    func showAlert(title: String = "",
        message: String,
        actions: [AlertActions] = [.standard(Constants.okAction, nil)],
        preferredStyle: UIAlertController.Style = .alert,
        withPreferredAction: Bool = false
    ) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: preferredStyle)
        let alertActions = actions.map { action in
            action.action
        }
        alertActions.forEach(alertController.addAction)
        if withPreferredAction {
            if #available(iOS 9.0, *) {
                alertController.preferredAction = alertActions[0]
            }
        }
        rootController?.present(alertController, animated: true, completion: nil)
    }
}
