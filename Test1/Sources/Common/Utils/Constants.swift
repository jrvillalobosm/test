//
//  Constants.swift
//  Test1
//
//  Created by appleDeveloper on 29/03/20.
//  Copyright © 2020 Scotiabank. All rights reserved.
//

import Foundation

enum Constants {
    static let locationEnabledTitle = "Activa Localización para que \"EcoBici\" pueda determinar tu ubicación"
    static let locationEnabledMsg = "EcoBici utiliza tu ubicación para ayudarte a encontrar estaciones cercanas."
    static let locationDeniedTitle = "Ubicación"
    static let locationDeniedMsg = "Para acceder a las estaciones cercanas a ti, EcoBici necesita acceso a tu ubicación actual."
    static let cancelAction = "Cancelar"
    static let changeSettingsAction = "Cambiar configuración"
    static let settingsAction = "Configuración"
    static let okAction = "Aceptar"
}
