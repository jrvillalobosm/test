//
//  ExtraInfoStation.swift
//  Test1
//
//  Created by appleDeveloper on 29/03/20.
//  Copyright © 2020 Scotiabank. All rights reserved.
//

import Foundation
import ObjectMapper

class ExtraInfoStation: Mappable {
    private(set) var uid: Int64!
    private(set) var address: String!
    private(set) var districtCode: String!
    private(set) var status: String!
    private(set) var zip: String!
    private(set) var nearbyStationList: [Int] = []

    required init?(map: Map) { }

    func mapping(map: Map) {
        uid <- map["uid"]
        address <- map["address"]
        districtCode <- map["districtCode"]
        status <- map["status"]
        zip <- map["zip"]
        nearbyStationList <- map["NearbyStationList"]
    }
}
