//
//  Station.swift
//  Test1
//
//  Created by appleDeveloper on 29/03/20.
//  Copyright © 2020 Scotiabank. All rights reserved.
//

import Foundation
import MapKit
import ObjectMapper

class Station: Mappable {
    private(set) var id: String!
    private(set) var location: CLLocation!
    private(set) var name: String!
    private(set) var timestamp: String!
    private var freeBikes: Int!
    private var emptySlots: Int!
    private(set) var extra: ExtraInfoStation!
    public var distance: Double?

    public var hasFreeBikes: Bool {
        get {
            return freeBikes != nil && freeBikes != 0
        }
    }

    public var formatDistance: String {
        guard let distance = distance else {
            return "Error"
        }
        if distance > 1000 {
            return String(format: "%.2f km", distance / 1000)
        } else {
            return String(format: "%.0f mts", distance)
        }
    }

    public var freeBikesText: String {
        return String(format: "Bicis disponibles: %d", freeBikes != nil ? freeBikes : 0)
    }

    public var emptySlotsText: String {
        return String(format: "Lugares disponibles: %d", emptySlots != nil ? emptySlots : 0)
    }

    required init?(map: Map) { }

    func mapping(map: Map) {
        var latitude: Double?
        var longitude: Double?
        id <- map["id"]
        name <- map["name"]
        timestamp <- map["timestamp"]
        freeBikes <- map["free_bikes"]
        emptySlots <- map["empty_slots"]
        extra <- map["extra"]
        latitude <- map["latitude"]
        longitude <- map["longitude"]
        if let latitude = latitude, let longitude = longitude {
            location = CLLocation(latitude: latitude, longitude: longitude)
        }
    }
}
