//
//  Stations.swift
//  Test1
//
//  Created by appleDeveloper on 29/03/20.
//  Copyright © 2020 Scotiabank. All rights reserved.
//

import Foundation
import ObjectMapper

class Stations: Mappable {
    private(set) var stations: [Station] = []

    required init?(map: Map) { }

    func mapping(map: Map) {
        stations <- (map["network"], StationsArrayTransformType())
    }
}

private class StationsArrayTransformType: TransformType {
    typealias Object = [Station]
    typealias JSON = [[String: Any]]

    public func transformFromJSON(_ value: Any?) -> Object? {
        guard let network = value as? [String: Any],
            let stations = network["stations"] as? JSON else { return nil }
        return stations.compactMap({ Station(JSON: $0) })
    }

    func transformToJSON(_ value: [Station]?) -> JSON? {
        guard let stations = value else { return nil }
        return stations.map { $0.toJSON() }
    }
}
