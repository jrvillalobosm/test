//
//  StationListInteractor.swift
//  Test1
//
//  Created by appleDeveloper on 29/03/20.
//  Copyright © 2020 Scotiabank. All rights reserved.
//

import Foundation
import MapKit

class StationListInteractor: NSObject {
    public weak var presenter: StationsListPresenter?
    fileprivate(set) var location: CLLocation?
    fileprivate var locationService = LocationService()

    override init() {
        super.init()
        self.addObservers()
    }

    public func getData() {
        if location != nil {
            locationService.stop()
        }
        locationService.delegate = self
        locationService.start()
    }

    public func stopData() {
        if location != nil {
            locationService.stop()
        }
        removeObservers()
    }

    @objc private func appMovedToBackground() {
        getData()
    }

    private func addObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(appMovedToBackground), name: UIApplication.willEnterForegroundNotification, object: nil)
    }

    private func removeObservers() {
        NotificationCenter.default.removeObserver(self, name: UIApplication.willEnterForegroundNotification, object: nil)
    }

    fileprivate func dataDidFetch() {
        guard let presenter = presenter else { return }
        let api = ApiManager()
        api.getData { stations in
            let sortedStations = stations
                .stations
                .filter({ $0.hasFreeBikes })
                .map { station -> Station in
                    self.distance(station)
                    return station
                }.sorted(by: { $1.distance == nil || $0.distance != nil && $0.distance! < $1.distance! })
            presenter.dataDidFetch(data: sortedStations)
        }
    }

    private func distance(_ station: Station) {
        guard let location = location else { return }
        station.distance = location.distance(from: station.location)
    }
}

extension StationListInteractor: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        // presenter?.showAlert
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let lastLocation = locations.last else { return }
        location = lastLocation
        dataDidFetch()
    }
}
