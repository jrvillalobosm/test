//
//  StationListPresenter.swift
//  Test1
//
//  Created by appleDeveloper on 29/03/20.
//  Copyright © 2020 Scotiabank. All rights reserved.
//

import Foundation
import UIKit

class StationsListPresenter {
    public var router: StationsListRouter?
    public weak var view: StationListViewController?
    public var interactor: StationListInteractor?

    func showNearByStation(with element: Station, from view: UIViewController) {

    }

    func viewDidLoad() {
        loadData()
    }

    func viewDidDisappear() {
        guard let interactor = interactor else { return }
        interactor.stopData()
    }

    func dataDidFetch(data: [Station]) {
        guard let view = view else { return }
        view.reloadStations(with: data)
    }

    private func loadData() {
        guard let interactor = interactor else { return }
        interactor.getData()
    }
}
