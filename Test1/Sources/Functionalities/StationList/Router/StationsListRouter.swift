//
//  StationsListRouter.swift
//  Test1
//
//  Created by appleDeveloper on 29/03/20.
//  Copyright © 2020 Scotiabank. All rights reserved.
//

import Foundation
import UIKit

class StationsListRouter {
    func pushToNearByStation(with element: Station, from view: UIViewController) {

    }
}

extension StationsListRouter {
    class func createStationsListModule(view: StationListViewController) {
        let presenter = StationsListPresenter()
        let interactor = StationListInteractor()
        interactor.presenter = presenter
        presenter.router = StationsListRouter()
        presenter.view = view
        presenter.interactor = interactor
        view.presenter = presenter
    }
}
