//
//  StationListViewController.swift
//  Test1
//
//  Created by appleDeveloper on 29/03/20.
//  Copyright © 2020 Scotiabank. All rights reserved.
//

import UIKit

fileprivate let verticalSpaceCells: CGFloat = 12.0

class StationListViewController: UIViewController {
    @IBOutlet weak var stationTableView: UITableView!
    fileprivate var stationsList: [Station] = [Station]()
    public var presenter: StationsListPresenter?

    override public func viewDidLoad() {
        super.viewDidLoad()
        self.stationTableView.register(UINib(nibName: "StationTableViewCell", bundle: nil), forCellReuseIdentifier: "StationTableViewCell")
        StationsListRouter.createStationsListModule(view: self)
        if let presenter = presenter {
            presenter.viewDidLoad()
        }
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        if let presenter = presenter {
            presenter.viewDidDisappear()
        }
    }

    func reloadStations(with stations: [Station]) {
        stationsList = stations
        stationTableView.reloadData()
        stationTableView.setContentOffset(CGPoint.zero, animated: true)
    }
}

extension StationListViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_: UITableView, heightForHeaderInSection _: Int) -> CGFloat {
        return verticalSpaceCells // 20.0
    }

    func tableView(_: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if section == self.stationsList.count - 1 {
            return verticalSpaceCells // 20.0
        }
        return CGFloat.leastNonzeroMagnitude
    }

    func numberOfSections(in _: UITableView) -> Int {
        return self.stationsList.count
    }

    func tableView(_: UITableView, numberOfRowsInSection _: Int) -> Int {
        if self.stationsList.isEmpty {
            return 0
        }
        return 1
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = stationTableView.dequeueReusableCell(withIdentifier: "StationTableViewCell", for: indexPath) as! StationTableViewCell
        let station = stationsList[indexPath.section]
        cell.addressLabel.text = station.extra.address
        cell.distanceLabel.text = station.formatDistance
        cell.freeBikesLabel.text = station.freeBikesText
        cell.emptySlotsLabel.text = station.emptySlotsText
        cell.layer.cornerRadius = 6
        cell.clipsToBounds = true
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let presenter = presenter else { return }
        presenter.showNearByStation(with: stationsList[indexPath.row], from: self)
    }
}
